package by.nikita.chaichits_l17_t1.rvAdapter

class AdapterItem {
    var time : String
    var action : String

    constructor(time: String, action: String) {
        this.time = time
        this.action = action
    }
}