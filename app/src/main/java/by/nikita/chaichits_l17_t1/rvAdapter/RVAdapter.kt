package by.nikita.chaichits_l17_t1.rvAdapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import by.nikita.chaichits_l17_t1.R


class RVAdapter: RecyclerView.Adapter<RVAdapter.ViewHolder>() {

    var adapterItems: ArrayList<AdapterItem> = ArrayList()

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.adapter_item, viewGroup, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(ViewHolder: ViewHolder, i: Int) {
        ViewHolder.time.text = adapterItems[i].time
        ViewHolder.action.text = adapterItems[i].action
    }

    override fun getItemCount(): Int {
        return adapterItems.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var time: TextView
        var action: TextView

        init {
            this.time = itemView.findViewById(R.id.tvTime)
            this.action = itemView.findViewById(R.id.tvAction)
        }
    }

    fun addItem (time : String, action : String){
        val item = AdapterItem(time, action)
        adapterItems.add(item)
        notifyDataSetChanged()
    }
}