package by.nikita.chaichits_l17_t1.notification

import android.content.Context
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import by.nikita.chaichits_l17_t1.R

const val CHANNEL_ID = "CHANNEL ID"

class NotificationCreator {

    fun createNotification (context : Context, id : Int, text : String){

        val builder = NotificationCompat.Builder(context, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_notifications)
            .setContentTitle(context.resources.getString(R.string.notificationTitle))
            .setContentText(text)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)

        with(NotificationManagerCompat.from(context)) {
            notify(id, builder.build())
        }
    }
}