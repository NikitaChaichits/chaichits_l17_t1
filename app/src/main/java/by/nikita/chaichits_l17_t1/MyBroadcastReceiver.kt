package by.nikita.chaichits_l17_t1


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import by.nikita.chaichits_l17_t1.notification.NotificationCreator
import by.nikita.chaichits_l17_t1.rvAdapter.RVAdapter
import java.text.SimpleDateFormat
import java.util.*

class MyBroadcastReceiver : BroadcastReceiver() {
    val adapter = RVAdapter()
    private val notifiction = NotificationCreator()

    override fun onReceive(context: Context, intent: Intent) {
        val action = intent.action
        val sdf = SimpleDateFormat("hh:mm")
        val time = sdf.format(Date())

        when (action) {
            Intent.ACTION_POWER_CONNECTED -> {
                adapter.addItem(time, "Зарядка подключена")
                notifiction.createNotification(
                    context, 1,
                    context.resources.getString(R.string.batteryOnText)
                )
            }
            Intent.ACTION_POWER_DISCONNECTED -> {
                adapter.addItem(time, "Зарядка отключена")
                notifiction.createNotification(
                    context, 2,
                    context.resources.getString(R.string.batteryOffText)
                )
            }
            Intent.ACTION_AIRPLANE_MODE_CHANGED -> {
                adapter.addItem(time, "Режим в самолете изменен")
                notifiction.createNotification(
                    context, 3,
                    context.resources.getString(R.string.airplaneNotificationText)
                )
            }
        }
    }


}